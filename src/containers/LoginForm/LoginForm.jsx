import React, { Component } from "react";
import { login } from '../../api/auth';

// 8 caráctere, mayúscula, minúsula y número;
const INITIAL_STATE = {
  email: "",
  password: "",
  error: "",
};

class LoginForm extends Component {
  state = INITIAL_STATE;

  handleSubmitForm = async ev => {
      ev.preventDefault();

      try {
        const data = await login(this.state);
        console.log('LOGIN COMPLETADO', data);
        this.props.saveUser(data);
        this.setState(INITIAL_STATE);
        this.props.history.push('/');
      } catch(error) {
        this.setState({ error: error.message });
      }
  }

  handleChangeInput = (ev) => {
    const { name, value } = ev.target;
    this.setState({ [name]: value });
  };

  render() {
    return (
      <form onSubmit={this.handleSubmitForm}>
        <h1>Formulario de Login</h1>
        <label htmlFor="email">
          <p>Email</p>
          <input
            type="text"
            name="email"
            value={this.state.email}
            onChange={this.handleChangeInput}
          />
        </label>

        <label htmlFor="password">
          <p>Contraseña</p>
          <input
            type="password"
            name="password"
            value={this.state.password}
            onChange={this.handleChangeInput}
          />
        </label>

        {this.state.error && <p style={{ color: 'red' }}>
          Ha ocurrido un error: {this.state.error}
        </p>}

        <div style={{ marginTop: '20px' }}>
            <button type="submit">Login</button>
        </div>
      </form>
    );
  }
}

export default LoginForm;
