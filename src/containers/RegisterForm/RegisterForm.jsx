import React, { Component } from "react";
import { register } from '../../api/auth';

const INITIAL_STATE = {
  username: "",
  email: "",
  password: "",
  error: "",
};

class RegisterForm extends Component {
  state = INITIAL_STATE;

  handleSubmitForm = async ev => {
      ev.preventDefault();

      try {
        const data = await register(this.state);
        console.log('REGISTRO COMPLETADO', data);
        this.props.saveUser(data);
        this.setState(INITIAL_STATE);
      } catch(error) {
        this.setState({ error: error.message });
      }
  }

  handleChangeInput = (ev) => {
    const { name, value } = ev.target;
    this.setState({ [name]: value });
  };

  render() {
    return (
      <form onSubmit={this.handleSubmitForm}>
        <h1>Register Form</h1>
        <label htmlFor="username">
          <p>Nombre de Usuario</p>
          <input
            type="text"
            name="username"
            value={this.state.username}
            onChange={this.handleChangeInput}
          />
        </label>

        <label htmlFor="email">
          <p>Email</p>
          <input
            type="text"
            name="email"
            value={this.state.email}
            onChange={this.handleChangeInput}
          />
        </label>

        <label htmlFor="password">
          <p>Contraseña</p>
          <input
            type="password"
            name="password"
            value={this.state.password}
            onChange={this.handleChangeInput}
          />
        </label>

        {this.state.error && <p style={{ color: 'red' }}>
          Ha ocurrido un error: {this.state.error}
        </p>}

        <div style={{ marginTop: '20px' }}>
            <button type="submit">Registo de cuenta</button>
        </div>
      </form>
    );
  }
}

export default RegisterForm;
