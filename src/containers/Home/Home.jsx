import React, { Component } from "react";
import { Link } from "react-router-dom";

class Home extends Component {
  render() {
    return (
      <div>
        <h1>¡Bienvenid@ a nuestra webapp en React desplegada!</h1>

        {!this.props.user && <div>
          <Link to="/login">Quiero iniciar sesión</Link>
        </div>}

        <div>
          <Link to="/register">Quiero registrarme</Link>
        </div>
      </div>
    );
  }
}

export default Home;
