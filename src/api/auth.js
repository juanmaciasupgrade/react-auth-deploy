console.log('PROCESS ENV TEST', process.env.REACT_APP_SERVER_URL)
const registerUrl = `${process.env.REACT_APP_SERVER_URL}/auth/register`;
const loginUrl = `${process.env.REACT_APP_SERVER_URL}/auth/login`;
const checkSessionUrl = `${process.env.REACT_APP_SERVER_URL}/auth/check-session`;

export const register = async (userData) => {
  const request = await fetch(registerUrl, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type":"application/json",
      'Access-Control-Allow-Credentials': true,
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
    body: JSON.stringify(userData),
  })

  
  const response = await request.json();

  if(!request.ok) {
    throw new Error(response.message);
  }

  return response;
};

export const login = async userData => {
  const request = await fetch(loginUrl, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "https://node-auth-upgrade.herokuapp.com/",
      "Access-Control-Allow-Credentials": true,
    },
    credentials: 'include',
    body: JSON.stringify(userData),
  })

  const response = await request.json();
  if(!request.ok) {
    return new Error(response.message);
  }

  return response;
}

export const checkSession = async () => {
  const request = await fetch(checkSessionUrl, {
    method: 'GET',
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    },
    credentials: 'include',
  });

  const response = await request.json();
  if(!request.ok) {
    throw new Error(response.message);
  }

  return response;
}