import { Component } from 'react';
import { BrowserRouter as Router , Switch, Route } from 'react-router-dom';
import { checkSession } from './api/auth';
import RegisterForm from './containers/RegisterForm';
import LoginForm from './containers/LoginForm';
import SecureRoute from './components/SecureRoute';
import PrivateComponent from './components/PrivateComponent';
import Home from './containers/Home';
import './App.scss';
// Ruta protegida:
// https://upgrade-auth.herokuapp.com/api/secret

class App extends Component {
  state = {
    error: '',
    hasUser: null,
  };

  componentDidMount() {
    this.checkUserSession();
  }

  async checkUserSession() {
    try {
      const data = await checkSession();
      delete data.password;
      this.setState({ user: data, hasUser: true });
    } catch(error) {
      this.setState({ error: error.message, hasUser: false });
    }
  }

  saveUser = user => {
    delete user.password;
    this.setState({ user, hasUser: true });
  };

  render() {
    return (
      <Router>
        {this.state.error && <p style={{ color: 'red', textAlign: 'center' }}>
          Ha ocurrido un error: {this.state.error}
        </p>}
        
        <div className="App">
          <Switch>
            <Route path="/register" component={props => <RegisterForm {...props} saveUser={this.saveUser} />} />
            <Route path="/login" component={props => <LoginForm {...props} saveUser={this.saveUser} />} />
            <SecureRoute path="/pepe" hasUser={this.state.hasUser} component={props => <PrivateComponent {...props} />} />
            <Route path="/" component={props => <Home {...props} user={this.state.user} />} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
